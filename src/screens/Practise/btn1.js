/* eslint-disable */
import React from 'react';
import { View, Button, Text, TouchableHighlight, StyleSheet, Alert, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';

const pressEvent = () => {
    Alert.alert('Button Pressed!!');
}

const Btn1 = () => {
    return (
        <View style={styles.container}>
            <TouchableHighlight onPress={pressEvent}>
                <View style={styles.button}>
                    <Text style={styles.buttonText}>TouchableHighlight</Text>
                </View>
            </TouchableHighlight>
            <TouchableOpacity onPress={pressEvent}>
                <View style={styles.button}>
                    <Text style={styles.buttonText}>TouchableOpacity</Text>
                </View>
            </TouchableOpacity>
            <TouchableWithoutFeedback onPress={ pressEvent }>
                <View style={styles.button}>
                    <Text style={styles.buttonText}>TouchableWithoutFeedback</Text>
                </View>
            </TouchableWithoutFeedback>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 60,
        alignItems: 'center'
    },
    button: {
        marginBottom: 30,
        width: 260,
        alignItems: 'center',
        backgroundColor: '#2196F3'
    },
    buttonText: {
        textAlign: 'center',
        padding: 20,
        color: 'white'
    }
});

export default Btn1;